# Vagrant box for Asqatasun 4  (deprecated)

This is a **deprecated** version of Asqatasun, 
we recommend you to use a more recent version: [Asqatasun v5](../5.x).
