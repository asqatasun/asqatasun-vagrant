# Asqatasun Box on Ubuntu 18.04 with local build

## 0. Place local Asqatasun binary

Locate the Asqatasun binary (`asqatasun*.tar.gz`) you've just built. Copy it into this directory (containing the vagrantFile).
Symlinks do not work, thus a copy is mandatory.

## Vagrant pre-requisites

```shell script
vagrant plugin install vagrant-disksize
```

See https://github.com/sprotheroe/vagrant-disksize

## 1. Outsite the box

Get into this directory, then:

```
vagrant up
vagrant ssh
```

## 2. Inside the box

```
sudo -i
/root/mailhog -smtp-bind-addr 0.0.0.0:25 &
cd /vagrant
./asqatasun.sh
```

The script ends with a `tail -f` on Asqatasun and Tomcat log files

## 3. Use it

* Use Asqatasun on http://localhost:8087/asqatasun/
* Emails sent by Asqatasun can be viewed (thanks to MailHog) on http://localhost:8025/

## 4. Destroy the box

When you're done, from outside the box:

```
vagrant destroy -f
```

