# Using Vagrant - Asqatasun 5.x

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Prerequisites

- [Vagrant](https://www.vagrantup.com/downloads)
- [VirtualBox](https://www.virtualbox.org/)

For Linux users (debian, ubuntu), do *not* install Vagrant and VirtualBox from `apt-get` (packages are way too old),
download .deb from the respective websites.

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| API      | 8081 | `http://localhost:8081`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tips: 

- You can customize some configurations with `VAGRANT_*` variables before the `vagrant up`.
- If you want to change some port numbers or remove localhost IP limitation, 
  you can use `VAGRANT_*_PORT` and `VAGRANT_*_IP` variables before the `vagrant up`.

## Choose version of Asqatasun

Choose the version of Asqatasun you want to start: `5.<minor>.<patch>` or  `5-SNAPSHOT`

```shell
git clone https://gitlab.com/asqatasun/asqatasun-vagrant
cd asqatasun-vagrant

# for Asqatasun 5.<minor>.<patch>
cd 5.x/5.<minor>.y/5.<minor>.<patch>/<final-directory>
        # "final directory" indicates the OS, database software and the JDK version.

# for Asqatasun 5-SNAPSHOT
cd 5.x/5-SNAPSHOT/
```


## Launch Asqatasun

- Creates and starts the VM (Virtual Machine)
- Stops gracefully the VM
- ...

```shell
# To be executed in the directory containing the Vagrantfile

# Creates and starts the VM (Virtual Machine) according to the Vagrantfile
vagrant destroy -f  # stops the running machine Vagrant and destroys all resources
vagrant up

    # Creates and starts the VM (Virtual Machine)
    # with some customizations (Firefox version, ports, memory allocated, ...)
    # - customize Firefox and Geckodriver versions
    # - customize webapp/api/db ports                ---> port allowed above 1000
    # - customize size of memory allocated to the VM ---> 2048 (default) | 4096 | 6144 | 8192
    # - customize number of CPU allocated to the VM  --->    1 (default) |    2 |    3 |    4
    vagrant destroy -f  # stops the running machine Vagrant and destroys all resources
    VAGRANT_FIREFOX_VERSION="91.1.0esr"  \
    VAGRANT_GECKODRIVER_VERSION="0.30.0" \
    VAGRANT_API_PORT=8888 \
    VAGRANT_APP_PORT=9999 \
    VAGRANT_DB_PORT=3333  \
    VAGRANT_MEMORY=4096   \
    VAGRANT_CPU=4         \
    vagrant up

# Stops gracefully the VM
vagrant halt  

# Restart the VM
vagrant up

# Stops the VM and destroys all resources
# that were created during the machine creation process.
vagrant destroy -f
```

- By default, the webapp is started.  
- If you also want to use the API, you have to use the following command: 
  ```shell
  # Start API server manually + see logs directly  (use Ctrl-C to stop it)
  vagrant ssh -c '00_bin/10_start_api.sh'
  ```

## Play with Asqatasun webapp

- In your browser, go to `http://127.0.0.1:8080/`
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`

## Play with Asqatasun API

- In your browser: `http://127.0.0.1:8081/`   (API documentation and **Swagger** playground)
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`

You can refer to [Asqatasun API documentation](https://doc.asqatasun.org/v5/en/Developer/API/) 
for full usage and tips on how to use it.

